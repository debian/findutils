# Repository for packaging of GNU findutils

This repository is hosted on salsa.debian.org's 
_debian group_ whose general policy is currently
*direct commits without coordination are welcome*.
However for this repository we would strongly appreciate if you sent us
a heads up by mail (@packages.debian.org) before pushing or use a merge
request.

Thank you.
